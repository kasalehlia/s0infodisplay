import json
import threading
from queue import Queue

from flask import Flask, render_template
from flask_socketio import SocketIO

import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app = Flask(__name__, static_url_path='', static_folder='public')
app.config['SECRET_KEY'] = 'Een3ooYiimePood5aeCe8ae0Eihohsh1'
socketio = SocketIO(app, async_mode='threading')

latest_updates = dict()

@socketio.on('ident')
def on_ident(json):
    #TODO better dependency resolution for outlets
    [socketio.emit('update', upd) for key,upd in latest_updates.items() if key[1] == '.']
    [socketio.emit('update', upd) for key,upd in latest_updates.items() if key[1] != '.']

queue = Queue()
def consumer():
    print('consumer')
    while True:
        event, data = queue.get()
        socketio.emit(event, data)
        latest_updates[(data['module'],data['outlet'])] = data

threading.Thread(target=consumer).start()


import modules
#TODO do not pollute namespace
from modules import *

def emit_factory(mod):
    def emit(content, outlet='.'):
        queue.put(('update', dict(outlet=outlet, module=mod, content=content)))
    return emit

def log_factory(mod):
    def log(err):
        print('[{}] ERR: {}'.format(mod, err))
    return log

for mod_name in modules.__all__:
    mod = getattr(modules, mod_name)
    print('starting module {}'.format(mod_name))
    threading.Thread(target=mod.run, args=(emit_factory(mod_name),log_factory(mod_name))).start()


@app.route('/')
def index():
    return app.send_static_file('index.html')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=8000)

