import re
import requests
from datetime import date, datetime, timedelta
from pystache import render
from time import sleep

from arrow import Arrow
import ics
import dateutil.rrule as rrule

URL = 'https://stratum0.org/kalender/termine.ics';

def run(emit, log_err):
    template = None
    with open('modules/calendar/template.mustache', 'r') as f:
        template = f.read()
    while True:
        try:
            r = requests.get(URL)
            cal = _fix_and_parse(r.text)
            one_day = timedelta(days=1)
            day = date.today()
            soon = []
            for _ in range(8*7):
                soon.extend(cal.events.on(Arrow.fromdate(day)))
                day += one_day
            emit(render(template, [_process_event(ev) for ev in soon][:8]))
        except Exception as e:
            log_err(e)
        sleep(60)

def _process_event(ev):
    now = datetime.now()
    begin = ev.begin.datetime.replace(tzinfo=None)
    end = ev.end.datetime.replace(tzinfo=None)
    e = dict(title=ev.name)
    if end < now:
        e['past'] = True
    elif begin <= now <= end:
        e['now'] = True
    if begin.date() == date.today():
        e['startRendered'] = begin.strftime('%H:%M')
    else:
        e['startRendered'] = begin.strftime('%A, %d.%m. %H:%M')
    if ev.duration < timedelta(days=1):
        e['endRendered'] = end.strftime('%H:%M')
    else:
        e['endRendered'] = end.strftime('%A %H:%M')
    e['duration'] = ':'.join(str(ev.duration).split(':')[:2])
    return e

def _fix_and_parse(ical):
    ical = ical.replace(';VALUE=DATE-TIME', '')
    cal = ics.Calendar(ical)
    # add recurring events since the stupid lib does not handle that
    start_day = datetime.now() - timedelta(hours=5)
    limit_day = datetime.today() + timedelta(days=8*7)
    add = []
    for ev in cal.events:
        m = re.search('\nRRULE:([^\n]+)\n', str(ev))
        if m:
            dtstart = ev.begin.datetime.replace(tzinfo=None)
            rule = rrule.rrulestr(m.group(1), dtstart=dtstart)
            duration = ev.duration
            for occ in rule.xafter(start_day):
                if occ > limit_day: break
                nev = ev.clone()
                nev.end = occ+duration
                nev.begin = occ
                add.append(nev)
    [cal.events.append(e) for e in add]
    return cal


