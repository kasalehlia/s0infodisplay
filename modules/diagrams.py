import json
import requests
from datetime import datetime
from pystache import render
from time import sleep

URL = 'http://shiny.tinyhost.de/php/getdata.php?time=1&id[]={}'

def _fetch_data(id):
    url = URL.format(id)
    r = requests.get(url)
    data = r.text.split('\n')
    out = []
    for line in data[1:]:
        l = line.split(',')
        if len(l) == 2:
            date = datetime.strptime(l[0], '%Y/%m/%d %H:%M:%S')
            out.append((int(date.timestamp()*1000), float(l[1])))
    return json.dumps(out)


def run(emit, log_err):
    lastchange = -1
    templates = dict()
    for name in ['data','main']:
        with open('modules/diagrams/{}.mustache'.format(name), 'r') as f:
            templates[name] = f.read()
    emit(render(templates['main'], {}))
    while True:
        try:
            power = _fetch_data(1)
            devices = _fetch_data(4)
            emit(render(templates['data'], dict(power=power, devices=devices)), outlet='data')
        except Exception as e:
            log_err(e)
        sleep(30)

