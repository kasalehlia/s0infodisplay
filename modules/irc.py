import socket
import ssl

CACHE_LIMIT = 35
HOST = 'bouncer.ksal.de'
PORT = 28921
AUTH_USER = 'infodisplay/Freenode'
AUTH_PW = 'Fagee9ie'
NICK = 'infodisplay'
CHANNEL = '#stratum0'

def _clean_str(s):
    return s.rstrip().replace('<','&lt;').replace('>','&gt;')

def run(emit, log_err):
    emit('<h3>&nbsp;IRC #stratum0</h3><div class="chat" data-infodisplay-outlet="inner"></div>')
    ctx = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1)
    while 1:
        cache = []
        sock = socket.socket()
        conn = ctx.wrap_socket(sock)
        conn.connect((HOST,PORT))
        fp = conn.makefile('rb')

        conn.write('PASS {}:{}\r\n'.format(AUTH_USER, AUTH_PW).encode())
        conn.write('NICK {}\r\nUSER {} 8 * :{}\r\n'.format(NICK, AUTH_USER, NICK).encode())
        conn.write('JOIN {}\r\n'.format(CHANNEL).encode())
        try:
            while 1:
                line = fp.readline().decode()
                if line[:4] == 'PING':
                    ans = line.replace('PING','PONG', 1)
                    conn.write(ans.encode())
                    continue
                source, command, args = line.split(' ', 2)
                if command == 'PRIVMSG' and 'znc@znc.in' not in source:
                    channel, message = args.split(' :', 1)
                    sender = source.split('!', 1)[0][1:]
                    message = message.strip()
                    if channel == CHANNEL:
                        if message[:7] == '\x01ACTION':
                            message = message.replace('ACTION ', '', 1)
                            out = '<p><span class="nobraces">{}</span> {}</p>'.format(sender, _clean_str(message))
                        else:
                            out = '<p><span>{}</span> {}</p>'.format(sender, _clean_str(message))
                        cache.append(out)
                        if len(cache) > CACHE_LIMIT:
                            cache.pop(0)
                        emit(''.join(cache), outlet='inner')
        except Exception as e:
            log_err(str(e))
        finally:
            log_err('Connection cycled')
            fp.close()
            conn.close()

