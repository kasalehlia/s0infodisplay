import time
from datetime import datetime

DATE_FORMAT = "%A %d.%m.%Y %H:%M:%S"

def run(emit, log_err):
    while True:
        try:
            start = time.time()
            out = datetime.now().strftime(DATE_FORMAT)
            out = '<div align="right"><h2>{}</h2></div>'.format(out)
            emit(out)
        except Exception as e:
            log_err(e)
        time.sleep(1-(time.time()-start))


