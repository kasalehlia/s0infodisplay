import json
import requests
from datetime import datetime
from pystache import render
from time import sleep

APPID = 'fdc3690e6f9a7572128fe4012b4a2500';
CITYID = '2945024';

DIRECTIONS = { 'NNE': 11.25, 'NE': 33.75, 'ENE': 56.25, 'E': 78.75, 'ESE': 101.25, 'SE': 123.75, 'SSE': 146.25, 'S': 168.75, 'SSW': 191.25, 'SW': 213.75, 'WSW': 236.25, 'W': 258.75, 'WNW': 281.25, 'NW': 303.75, 'NNW': 326.25, 'N': 348.75 };
ICON_BASE_URL = 'http://openweathermap.org/img/w/';

WEATHER_URL = 'http://api.openweathermap.org/data/2.5/weather?units=metric&id={}&appid={}'
FORECAST_URL = 'http://api.openweathermap.org/data/2.5/forecast?units=metric&id={}&appid={}'

def run(emit, log_err):
    template = None
    with open('modules/weather/template.mustache', 'r') as f:
        template = f.read()
    while True:
        try:
            current = fetch_weather()
            forecast = fetch_forecast(6)
            emit(render(template, dict(current=current, forecast=forecast)))
        except Exception as e:
            log_err(e)
        sleep(60*10)

def fetch_weather():
    r = requests.get(WEATHER_URL.format(CITYID, APPID))
    dat = json.loads(r.text)
    return {
        'temp': dat['main']['temp'],
        'wind': {'speed': dat['wind']['speed'], dir: deg_to_direction(dat['wind']['deg'])},
        'pressure': dat['main']['pressure'],
        'humidity': dat['main']['humidity'],
        'main': dat['weather'][0]['main'],
        'desc': dat['weather'][0]['description'],
        'icon': '{}{}.png'.format(ICON_BASE_URL, dat['weather'][0]['icon'])
    }

def fetch_forecast(count):
    r = requests.get(FORECAST_URL.format(CITYID, APPID))
    dat = json.loads(r.text)
    out = []
    for i in range(count):
        d = dat['list'][i]
        out.append({
            'time': datetime.fromtimestamp(d['dt']).strftime('%H:%M'),
            'temp': d['main']['temp'],
            'wind': {'speed': d['wind']['speed'], dir: deg_to_direction(d['wind']['deg'])},
            'main': d['weather'][0]['main'],
            'desc': d['weather'][0]['description'],
            'icon': '{}{}.png'.format(ICON_BASE_URL, d['weather'][0]['icon'])
        })
    return out

#        const date = new Date(d.dt * 1000);
#        return {
#          time: `${pad(date.getHours(), 2)}:${pad(date.getMinutes(), 2)}`,
#          temp: d.main.temp,
#          wind: { speed: d.wind.speed, dir: degToDirection(d.wind.deg) },
#          main: d.weather[0].main,
#          desc: d.weather[0].description,
#          icon: `${iconBaseURL + d.weather[0].icon }.png`,
#        };

def deg_to_direction(deg):
    dir = 'N'
    for k,v in DIRECTIONS.items():
        if deg > v:
            dir = k
    return dir
