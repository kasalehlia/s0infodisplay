import json
import requests
from datetime import datetime
from pystache import render
from time import sleep

URL = 'https://stratum0.org/status/status.json';

def run(emit, log_err):
    lastchange = -1
    templates = dict()
    for name in ['status','template']:
        with open('modules/brand/{}.mustache'.format(name), 'r') as f:
            templates[name] = f.read()
    emit(render(templates['template'], {}))
    while True:
        try:
            r = requests.get(URL)
            status = json.loads(r.text)
            state = status['state']
            if state['lastchange'] != lastchange:
                lastchange = state['lastchange']
                state['since'] = datetime.fromtimestamp(lastchange).strftime('%a, %H:%M')
                emit(render(templates['status'], state), outlet='status')
        except Exception as e:
            log_err(e)
        sleep(20)


