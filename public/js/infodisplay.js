var setIntervals = [];
var setIntervalBackup = window.setInterval;
window.setInterval = function (fun, delay) {
    setIntervals.push(setIntervalBackup(fun, delay));
}

$(function () {
    var socket = io.connect();
    socket.on('meta', function (cmd) {
        if (cmd === 'reload') {
            document.location.reload();
        }
    });
    socket.on('connect', function () {
        var ident = sessionStorage.getItem('ident');
        if (!ident) {
            ident = Math.random().toString().substr(2);
            sessionStorage.setItem('ident', ident);
        }
        socket.emit('ident', ident);
    });
    socket.on('disconnect', function () {
        setIntervals.forEach(function (interval) {
            window.clearInterval(interval);
        });
        setIntervals = [];
    });
    socket.on('update', function (data) {
        //console.log(data);
        var target = $('#'+data.module);
        if (data.outlet !== '.') {
            target = target.find('[data-infodisplay-outlet="'+data.outlet+'"]');
        }
        //console.log(target);
        if (target.length === 0) {
            console.warn('could not find outlet');
        }
        target.html(data.content);
        target.trigger('content', data.outlet);
    });
});
