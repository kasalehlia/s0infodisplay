module.exports = {
  extends: 'marudor/noReact',
  env: {
    node: true,
  },
  globals: {
    pad: false,
    DOW: false,
  },
  rules: {
    'header/header': 0,
  }
}
